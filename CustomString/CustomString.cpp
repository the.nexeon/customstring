// CustomString.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <string>
#include <iostream>

class String
{
public:
	/*
	/ Пустой конструктор (по-умолчанию) без параметров
	*/
	String() : _length(0)
	{
		_array = new char[0]; // Инициализируем пустой массив
	}

	/*
	/ Конструктор копирования из аналогичного класса
	*/
	String(const String& s) : _length(s._length) { 
		_array = new char[s._length]; // Создаем массив аналогичной длины, что и старый

		for (int i = 0; i < s._length; i++) {
			_array[i] = s._array[i]; // Копируем каждый символ из исходного массива в новый
		}
	}
		
	/*
	/ Конструктор, копирующий содержимое из массива char[]
	*/
	String(char c[])
	{
		size_t n = 0; // Итератор
		while (c[n] != '\0') n++; // Считаем колличество символов в исходной строке

		_length = n; // Указываем новую длину строки
		_array = new char[n]; // Инициализируем массив

		for (int j = 0; j < n; j++)
			_array[j] = c[j]; // Копируем каждый символ из исходного массива в новый
	}

	/*
	/ Конструктор, копирующий содержимое из const char*
	*/
	String(const char *c)
	{
		if (c != nullptr) { // Если указатель не пустой

			int n = 0; // Итератор
			while (c[n] != '\0') n++; // Считаем колличество символов в исходной строке

			_length = n; // Указываем новую длину строки
			_array = new char[n]; // Инициализируем массив

			for (int j = 0; j < n; j++)
				_array[j] = c[j]; // Копируем каждый символ из исходного массива в новый
		}
		else {
			_length = 0; // Сбрасываем длину строки
			_array = new char[0]; // Создаем пустой массив
		}
	}

	/*
	/ Конструктор, копирующий содержимое из std∷string
	*/
	String(const std::string &str) 
		: String(str.c_str()) // Вызываем другой конструктор 
	{
		
	}

	friend std::ostream& operator<< (std::ostream& so, const String& s);

	/*
	/ Длина строки
	*/
	size_t len() const {
		return _length;
	}

	/*
	/ Сброс длины строки
	*/
	void clear()
	{
		if (_array != nullptr) { // Если указатель не пустой
			delete[] _array; 
			_length = 0; // Сброс длины строки
			_array = new char[0]; // Создаем пустой массив 
		}
	}

	/*
	/ Конкатенация строки со строкой const char*
	*/
	void add(const char* c) {
		String temp(c);
		
		int newLength = _length + temp._length;

		char* newArray = new char[newLength];

		for (int i = 0; i < _length; i++) {
			newArray[i] = _array[i];
		}

		for (int j = 0; j < temp._length; j++) {
			newArray[_length + j] = temp._array[j];
		}

		delete[] _array;

		_length = newLength;
		_array = newArray;
	}

	//void add(const std::string&);
	//void insert(size_t, char*);
	//void cut(size_t, size_t);

	/*
	/ Деструктор
	*/
	~String() { 
		delete[] _array;
	}

private:
	// Длина строки
	size_t _length;

	// Указатель на массив символов
	char* _array;
};

 /*
 / Перезагрузка оператора << для вывода в консоль через std::cout
*/
std::ostream& operator<< (std::ostream& os, const String& s)
{
	if (s.len() > 0)
	{
		for (int j = 0; j < s.len(); j++)
			os << s._array[j];

		os << std::endl;
	}
	else os << "";

	return os;
}


int main()
{
	/* Конструкторы */

	String s1;

	char c1[] = {'H', 'e', 'l', 'l', 'o', '\0'};
	String s2(c1);

	const char* c2 = "World";
	String s3(c2);

	std::string str("Hello");
	String s4(str);

	String s5(s4);

	std::cout << s3;

	/* Методы */

	s2.add(" World!");
	
	std::cout << s2;

	system("pause");

    return 0;
}

